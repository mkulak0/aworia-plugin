<?php
add_action('wp_ajax_save_options', 'save_handler');
function save_handler(){

    update_option("aworia_id", $_POST['aworia_id']);
    update_option("aworia_pin", $_POST['aworia_pin']);
    update_option("aworia_env", $_POST['aworia_env']);
    update_option("aworia_login", $_POST['aworia_login']);
    update_option("aworia_password", openssl_encrypt($_POST['aworia_password'], "AES-256-OFB", "DUPA"));

    $link = get_site_url()."/wp-admin";
    echo <<<HTML
    <a href="$link">Wróć do panelu administracyjnego</a><br>
    Status (jeśli zero to brak błędów): 
HTML;
}