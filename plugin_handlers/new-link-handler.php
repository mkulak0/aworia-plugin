<?php
add_action('wp_ajax_nopriv_new_link', 'new_link_handler');
add_action('wp_ajax_new_link', 'new_link_handler');


function new_link_handler(){
    function GenerateChk($DotpayPin, $pid){
        $chk = $DotpayPin.$pid;
        return hash('sha256', $chk);
    }
    $DotpayId = get_option("aworia_id");
    $DotpayPin = get_option("aworia_pin");
    $Environment = get_option("aworia_env");
    $RedirectionMethod = "POST";

    $link_to_admin = get_site_url()."/wp-admin";


    $ParametersArray = [
        "amount" => (string)intval($_POST['amount']).".00",
        "currency" => "PLN",
        "description" => $_POST['description'],
        "url" => get_site_url()."?payment=ok",
        "buttontext" => "Wróć do aworia.pl",
        "urlc" => admin_url('admin-ajax.php')."?action=response_handler",
        "control" => (string)$_POST['control'],
    ];

    if($Environment == "production"){
        $part_of_link = "s2";
    } else if($Environment == "test"){
        $part_of_link = "test_seller";
    } else {
        $part_of_link = "???";
    }

    $link = "https://ssl.dotpay.pl/$part_of_link/api/v1/accounts/$DotpayId/payment_links/";


    $username = get_option("aworia_login");
    $password = openssl_decrypt(get_option("aworia_password"), "AES-256-OFB", "DUPA");

    $handle = curl_init($link);

    curl_setopt($handle, CURLOPT_POST, true );
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode((object)$ParametersArray));
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($handle, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen(json_encode((object)$ParametersArray)))                                                                       
    );

    $output = json_decode(curl_exec($handle));
    curl_close($handle);
    $new_link = $output->payment_url."&chk=".GenerateChk($DotpayPin, $output->token);

    echo <<<HTML
    Wygenerowany link dla zlecenia <b>${_POST['control']}</b> na kwotę <b>${_POST['amount']}.00</b> o tytule <b>${_POST['description']}</b><br>
    <input type="text" id="link" value="$new_link" style="width: 100%" /><br>
    <button onclick="myFunction()">Skopiuj do schowka</button><br>

    <a href="$link_to_admin">Wróć do panelu administracyjnego</a><br>
    Status (jeśli zero to brak błędów): 
    <script>
        function myFunction(){
            var copyText = document.getElementById("link");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
        }
    </script>
HTML;
}