<?php

add_action('wp_ajax_nopriv_form_handler', 'form_handler');
add_action('wp_ajax_form_handler', 'form_handler');

function form_handler(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $id = wp_insert_post([
        'post_type' => 'formularz',
        'post_title' => $data['name'].' | '.$data['phone'].' | '.$data['email'],
        'post_content' => $data['text']
    ]);
    echo $id != 0 ? true : false;
}