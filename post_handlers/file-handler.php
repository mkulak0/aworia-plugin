<?php
add_action('wp_ajax_nopriv_file_handler', 'file_handler');
add_action('wp_ajax_file_handler', 'file_handler');

function file_handler(){
    $data = (array)json_decode(file_get_contents('php://input'));

    $basepath = WP_CONTENT_DIR.(string)"/files/".$_REQUEST['id'].'/';
    mkdir($basepath, 0777);

    foreach($_FILES as $file){
        move_uploaded_file($file['tmp_name'], $basepath.$file['name']);
    }

    wp_send_json('ok');

    /* wp_send_json([
        'post' => $_REQUEST,
        'data' => $_FILES,
    ]); */
}