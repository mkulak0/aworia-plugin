<?php 
add_action('wp_ajax_nopriv_zlecenie_handler', 'zlecenie_handler');
add_action('wp_ajax_zlecenie_handler', 'zlecenie_handler');

function zlecenie_handler(){
    $data = (array)json_decode(file_get_contents('php://input'));

    $trans_priority = [
        'slow' => 'standardowy',
        'medium' => 'przyspieszony',
        'immediate' => 'pilny',
        'extreme' => 'ekspresowy'
    ];

    $trans_st = [
        "no" => "nie",
        "yes" => "tak",
        "unknown" => "nie-wiem",
        "none" => "brak"
    ];

    $trans_tot = [
        "swear" => "przysiegle",
        "usual" => "zwykle",
        "unknown" => "nie-wiem",
        "none" => "brak"
    ];
    
    $priority = $trans_priority[$data[termSelected]];
    $special_translation = $trans_st[$data[transSpecial]];
    $type_of_translation = $trans_tot[$data[transType]];
    $delivery_type = $data[mode] == 'net' ? 'internet' : "poczta";


    $id = wp_insert_post([
        'post_type' => 'job'
    ]);

    wp_insert_post([
        'ID' => $id,
        'post_title' => 'Zlecenie #'.$id,
        'post_type' => 'job'
    ]);

    if($delivery_type == "internet"){
        wp_set_object_terms($id, [$priority], 'priority');
        wp_set_object_terms($id, [$special_translation], 'special_translation');
        wp_set_object_terms($id, [$type_of_translation], 'type_of_translation');
    }
    wp_set_object_terms($id, [$delivery_type], 'delivery_type');

    $client_info = [
        'email' => $data['clientEmail'],
        'phone' => $data['clientPhone']
    ];

    $delivery_info = array_merge((array)$data['delivery'], ['postType' => $data['postType'] == 'private' ? "Osoba prywatna" : "Firma"]);

    $bill_info = array_merge((array)$data['billValues'], [
        'billActive' => $data['bill'], 
        'cost' => $data['cost'],
        'transactionStatus' => false
    ]);

    $data_info = array_merge([
        "text" => $data['text'],
        "comment" => $data['comment'],
        "languageInput" => $data['languageInput'],
        "languageOutput" => (array)$data['languageOutput']
    ]);

    update_post_meta($id,'client_info' , $client_info);
    update_post_meta($id,'delivery_info' , $delivery_info);
    update_post_meta($id,'bill_info' , $bill_info);
    update_post_meta($id,'data_info' , $data_info);
    update_post_meta($id,'notes_info' , []);

    /* wp_send_json([
        'post' => $_REQUEST,
        'data' => $data,
        'files' => $_FILES
    ]); */
    wp_send_json($id);
}