<?php

add_action('wp_ajax_nopriv_payment_handler', 'payment_handler');
add_action('wp_ajax_payment_handler', 'payment_handler');

function payment_handler(){
	$DotpayId = get_option("aworia_id");
    $DotpayPin = get_option("aworia_pin");
    $Environment = get_option("aworia_env");
    $RedirectionMethod = "POST";
    $data = (array)json_decode(file_get_contents('php://input'));

    $ParametersArray = [
    
        //"api_version" => "dev",
        "amount" => (string)intval($data['cost']).".00",
        "currency" => "PLN",
        "description" => "Zapłata za tłumaczenie",
        "url" => get_site_url()."?payment=ok",
        //"type" => "0",
        "buttontext" => "Wróć do aworia.pl",
        "urlc" => admin_url('admin-ajax.php')."?action=response_handler",
        "control" => (string)$_GET['id'],
        //"firstname" => "Jan",
        //"lastname" => "Nowak",
        //"payer" => (object)["email" => $_GET['email']]
        //"street" => "Warszawska",
        //"street_n1" => "1",
        //"city" => "Krakow",
        //"postcode" => "12-345",
        //"phone" => "123456789",
        //"country" => "POL",
        //"ignore_last_payment_channel" => 1
    ];

    function GenerateChk($DotpayPin, $pid){

        $chk = $DotpayPin.$pid;
        return hash('sha256', $chk);
    }
    
    if($Environment == "production"){
        $part_of_link = "s2";
    } else if($Environment == "test"){
        $part_of_link = "test_seller";
    } else {
        $part_of_link = "???";
    }

    $link = "https://ssl.dotpay.pl/$part_of_link/api/v1/accounts/$DotpayId/payment_links/";


    $username = get_option("aworia_login");
    $password = openssl_decrypt(get_option("aworia_password"), "AES-256-OFB", "DUPA");

    $handle = curl_init($link);

    curl_setopt($handle, CURLOPT_POST, true );
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode((object)$ParametersArray));
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($handle, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen(json_encode((object)$ParametersArray)))                                                                       
    );

    $output = json_decode(curl_exec($handle));
    //$info = curl_getinfo($handle);
    curl_close($handle);

    //$output->return_link = $output->payment_url."&chk=".GenerateChk($DotpayPin, $output->token);
    
    wp_send_json([link => $output->payment_url."&chk=".GenerateChk($DotpayPin, $output->token)]);

}   