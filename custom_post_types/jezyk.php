<?php
//CPT
add_action( 'init', 'cpt_jezyk');
//Metabox
add_action('add_meta_boxes', 'languages_add_meta_box');
add_action('save_post','languages_save_meta_box_data');

function cpt_jezyk() {

	$labels = array(
		'name'                  => 'Języki',
		'singular_name'         => 'Język',
		'menu_name'             => 'Języki',
		'name_admin_bar'        => 'Języki',
		'attributes'            => 'Atrybuty',
		'all_items'             => 'Wszystkie języki',
		'add_new_item'          => 'Dodaj język',
		'add_new'               => 'Dodaj język',
		'new_item'              => 'Dodaj język',
		'edit_item'             => 'Edytuj język',
		'update_item'           => 'Aktualizuj język',
		'view_item'             => 'Zobacz język',
		'view_items'            => 'Zobacz języki',
		'search_items'          => 'Wyszukaj języki',
		'not_found'             => 'Nie znaleziono',
		'not_found_in_trash'    => 'Nie znaleziono',
		'items_list'            => 'Lista języków',
	);
	$args = array(
		'label'                 => 'Język',
		'description'           => '???',
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 1,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'				=> 'dashicons-editor-spellcheck',
	);
	register_post_type( 'language', $args );
}

/* Tylko dodaje metabox */
function languages_add_meta_box(){
	add_meta_box('languages', 'Języki', 'languages_meta_box_callback', 'language');
}

/* Zapisuje/nadpisuje dane */
function languages_save_meta_box_data($post_id){
	if(get_post($post_id)->post_type != 'language'){
		return;
	}
	$old_langs = get_post_meta( $post_id, 'languages');
	$langs = [];

	foreach($_POST as $key => $value){
		if(strpos($key, 'lang') === 0){
			if($value !== 'lang-'.get_post($post_id)->post_name){
				$langs[substr($key, 5)] = [];
			}

		}
	}
	
	foreach($_POST as $key => $value){
		if(strpos($key, 'price') === 0 && $value != null){
			$name = substr($key, 6);
			if(array_key_exists($name, $langs)){
				$langs[$name]['price'] = $value;
			}
		}
	}

	foreach($_POST as $key => $value){
		if(strpos($key, 'ID') === 0 && $value != null){
			$name = substr($key, 3);
			if(array_key_exists($name, $langs)){
				$langs[$name]['ID'] = $value;
			}
		}
	}

	foreach($_POST as $key => $value){
		if(strpos($key, 'swear') === 0 && $value != null){
			$name = substr($key, 6);
			if(array_key_exists($name, $langs)){
				$langs[$name]['swear'] = $value;
			}
		}
	}

	update_post_meta( $post_id, 'languages', $langs);
}

/* Wyświetla dane w dashboardzie */
function languages_meta_box_callback($post){
	$post_langs = get_post_meta( $post->ID, 'languages', true);

	$query =  new WP_Query([
		'post_type' => 'language'
	]);
	$all_langs = $query->posts;

	$i = 0;
	foreach($all_langs as $all_lang){
		if($all_lang->post_name === $post->post_name){
			continue;
		}
		?>
		<div>
			<input type="hidden" value="<?php echo $all_lang->ID ?>"
				name="<?php echo 'ID-'.$all_lang->post_name ?>">
			<input type="checkbox" 
					name="<?php echo 'lang-'.$all_lang->post_name ?>" 
					id="<?php echo $i ?>" 
					<?php echo array_key_exists($all_lang->post_name, $post_langs) ? "checked" : ''?>
				>


			<label for="<?php echo $i ?>">
				<?php echo $all_lang->post_title ?>
			</label>

			| Tłumaczenie zwykłe:
			<input type="number" 
				name="<?php echo 'price-'.$all_lang->post_name ?>"
				value="<?php echo $post_langs[$all_lang->post_name]['price'] ?>"	
				style="max-width: 75px"
			>

			| Tłumaczenie przysięgłe:
			<input type="number" 
				name="<?php echo 'swear-'.$all_lang->post_name ?>"
				value="<?php echo $post_langs[$all_lang->post_name]['swear'] ?>"	
				style="max-width: 75px"
			>
		</div>
		<hr>
		<?php
		$i++;
	}
}