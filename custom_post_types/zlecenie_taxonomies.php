<?php

add_action('init', 'job_taxonomy');

function job_taxonomy() {

    /* Typ otrzymania internet/poczta */
	$labels = array(
		'name'                       => 'Typ otrzymania',
		'singular_name'              => 'Typ otrzymania',
		'menu_name'                  => 'Typ otrzymania',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                  => true,
        'capabilities' => [
            'manage_terms' => 'manage_delivery_type',
            'edit_terms' => 'edit_delivery_type',
            'delete_terms' => 'delete_delivery_type',
            'assign_terms' => 'assign_delivery_type',
        ]
	);
    register_taxonomy( 'delivery_type', array( 'job' ), $args );
    
    /* Termin realizacji czyli Ekspresowy/Pilny/Standardowy/Przyspieszony */
	$labels = array(
		'name'                       => 'Termin realizacji',
		'singular_name'              => 'Termin realizacji',
		'menu_name'                  => 'Termin realizacji',
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                  => true,
        'capabilities' => [
            'manage_terms' => 'manage_priority',
            'edit_terms' => 'edit_priority',
            'delete_terms' => 'delete_priority',
            'assign_terms' => 'assign_priority',
        ]
        /* 'meta_box_cb' => $view_function, */
	);
    register_taxonomy( 'priority', array( 'job' ), $args );


    /* Tłumaczenie specjalistyczne */
    $labels = array(
		'name'                       => 'Tłumaczenie specjalistyczne',
		'singular_name'              => 'Tłumaczenie specjalistyczne',
		'menu_name'                  => 'Tłumaczenie specjalistyczne',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities' => [
            'manage_terms' => 'manage_special_translation',
            'edit_terms' => 'edit_special_translation',
            'delete_terms' => 'delete_special_translation',
            'assign_terms' => 'assign_special_translation',
        ]
	);
    register_taxonomy( 'special_translation', array( 'job' ), $args );
    
    /* Rodzaj tłumaczenia */
    $labels = array(
		'name'                       => 'Rodzaj tłumaczenia',
		'singular_name'              => 'Rodzaj tłumaczenia',
		'menu_name'                  => 'Rodzaj tłumaczenia',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities' => [
            'manage_terms' => 'manage_type_of_translation',
            'edit_terms' => 'edit_type_of_translation',
            'delete_terms' => 'delete_type_of_translation',
            'assign_terms' => 'assign_type_of_translation',
        ]
	);
    register_taxonomy( 'type_of_translation', array( 'job' ), $args );

    /* Język źródłowy */
    /* function input_language_taxonomy_view($post, $box){
        $name = get_post_meta($post->ID, 'input_language', true);
        echo $name;
    }
    
    $labels = array(
        'name'                       => 'Język źródłowy',
        'singular_name'              => 'Język źródłowy',
        'menu_name'                  => 'Język źródłowy',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities' => [
            'manage_terms' => 'manage_input_language',
            'edit_terms' => 'edit_input_language',
            'delete_terms' => 'delete_input_language',
            'assign_terms' => 'assign_input_language',
        ],
        'meta_box_cb' => 'input_language_taxonomy_view'
    );
    register_taxonomy( 'input_language', array( 'job' ), $args ); */

    /* Języki docelowe */
    /* $labels = array(
        'name'                       => 'Języki docelowe',
        'singular_name'              => 'Języki docelowe',
        'menu_name'                  => 'Języki docelowe',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities' => [
            'manage_terms' => 'manage_output_languages',
            'edit_terms' => 'edit_output_languages',
            'delete_terms' => 'delete_output_languages',
            'assign_terms' => 'assign_output_languages',
        ]
    );
    register_taxonomy( 'output_languages', array( 'job' ), $args ); */


}

