<?php 

add_action('add_meta_boxes', 'add_meta_boxes');

function add_meta_boxes(){
    add_meta_box('client_info', 'Dane Zamawiającego', 'client_info_meta_box', 'job');
    add_meta_box('delivery_info', 'Dane Przesyłki', 'delivery_meta_box', 'job');
    add_meta_box('bill_info', 'Dane Faktury', 'bill_meta_box', 'job');
    add_meta_box('data_info', 'Dane dotyczące tłumaczenia', 'data_meta_box', 'job');
    add_meta_box('file_info', 'Załączone pliki', 'files_meta_box', 'job');
    add_meta_box('notes_info', 'Notatki', 'notes_meta_box', 'job');


}

/* Wyświetlanie danych o kliencie */
require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/client_info.php'); 

/* Dane przesyłki */

require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/delivery_info.php'); 

/* Wyświetlanie danych dotyczących faktury */

require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/bill_info.php'); 

/* Dane otrzymane od klienta */
require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/data_info.php'); 

/* Pliki */
require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/file_info.php'); 

/* Notatki */
require_once(plugin_dir_path(__FILE__).'zlecenie_metaboxes_parts/notes_info.php'); 