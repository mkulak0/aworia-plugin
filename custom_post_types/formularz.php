<?php

add_action( 'init', 'cpt_formularz');

function cpt_formularz(){
    $labels = array(
		'name'                  => 'Formularz',
		'singular_name'         => 'Formularz',
		'menu_name'             => 'Formularze',
		'name_admin_bar'        => 'Formularze',
		'attributes'            => 'Atrybuty',
		'all_items'             => 'Wszystkie Formularze',
		'add_new_item'          => 'Dodaj Formularz',
		'add_new'               => 'Dodaj Formularz',
		'new_item'              => 'Dodaj Formularz',
		'edit_item'             => 'Edytuj Formularz',
		'update_item'           => 'Aktualizuj Formularz',
		'view_item'             => 'Zobacz Formularz',
		'view_items'            => 'Zobacz Formularze',
		'search_items'          => 'Wyszukaj Formularze',
		'not_found'             => 'Nie znaleziono',
		'not_found_in_trash'    => 'Nie znaleziono',
		'items_list'            => 'Lista Formularzów',
	);
	$args = array(
		'label'                 => 'Formularz',
		'description'           => '???',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 1,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'				=> 'dashicons-media-text',
	);
	register_post_type( 'formularz', $args );
}