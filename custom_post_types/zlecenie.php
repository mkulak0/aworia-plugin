<?php
add_action( 'init', 'cpt_job');

function job_buttons() {
    $screen = get_current_screen();
    if( 'job' == $screen->post_type
        && 'edit' == $screen->base ){
        $link = admin_url("admin.php?page=aworia-search");
        ?>
        <div class="updated">
			<h2>
				<a href="<?php echo $link ?>">Wyszukiwanie zaawansowane</a>
			</h2>
        </div>
        <?php
    }
}
add_action( 'in_admin_header', 'job_buttons' );

function job_state(){
	echo "Lorem ipsum dolor sit amet";
}

add_action('post_submitbox_misc_actions', 'job_state');

function cpt_job() {

    $labels = array(
        'name'                  => 'Zlecenia',
        'singular_name'         => 'Zlecenie',
        'menu_name'             => 'Zlecenia',
        'name_admin_bar'        => 'Zleceni',
        'archives'              => 'Zlecenia',
        'attributes'            => 'Atrybuty Zlecenia',
        'all_items'             => 'Wszystkie zlecenia',
        'add_new_item'          => 'Dodaj nowe zlecenie',
        'add_new'               => 'Dodaj nowe zlecenie',
        'new_item'              => 'Nowe zlecenie',
        'edit_item'             => 'Edytuj zlecenie',
        'update_item'           => 'Aktualizuj zlecenie',
        'view_item'             => 'Zobacz zlecenie',
        'view_items'            => 'Zobacz zlecenia',
        'search_items'          => 'Wyszukaj zlecenia',
        'not_found'             => 'Nie znaleziono',
        'not_found_in_trash'    => 'Nie znaleziono',
        'items_list'            => 'Lista zleceń',
        'items_list_navigation' => 'Nawigacja po zleceniach',
        'filter_items_list'     => 'Filtruj zlecenia',
    );
    $args = array(
        'label'                 => 'Zlecenie',
        'description'           => 'Opis',
        'labels'                => $labels,
        'supports'              => array('title', 'revisions'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 0,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'map_meta_cap'          => true,
        'capability_type'       => 'job',
        'capabilities' => array(
            'edit_post'              => 'edit_job',
			'read_post'              => 'read_job',
			'delete_post'            => 'delete_job',
			'create_posts'           => 'create_jobs',
			'edit_posts'             => 'edit_jobs',
			'edit_others_posts'      => 'manage_jobs',
			'publish_posts'          => 'manage_jobs',
			'read_private_posts'     => 'read',
			'read'                   => 'read',
			'delete_posts'           => 'manage_jobs',
			'delete_private_posts'   => 'manage_jobs',
			'delete_published_posts' => 'manage_jobs',
			'delete_others_posts'    => 'manage_jobs',
			'edit_private_posts'     => 'edit_jobs',
			'edit_published_posts'   => 'edit_jobs'
        ),
        'menu_icon'             => 'dashicons-format-chat',
    );
    register_post_type( 'job', $args );
}

require( plugin_dir_path(__FILE__) . 'zlecenie_taxonomies.php');
require( plugin_dir_path(__FILE__) . 'zlecenie_metaboxes.php');



