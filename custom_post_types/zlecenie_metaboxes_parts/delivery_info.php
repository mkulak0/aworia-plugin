<?php
function delivery_meta_box($post) {
    $data = get_post_meta( $post->ID, 'delivery_info', true);

    $translation = [
        'address' => 'Adres',
        'postCode' => 'Kod pocztowy',
        'city' => 'Miasto',
        'country' => 'Kraj',
        'phone' => 'Numer telefonu',
        'name' => 'Imię i Nazwisko',
        'postType' => "Zleceniodowca"
    ];
    $by = [];
    if($data['byEmail']){
        $by[] = "Email";
    }
    if($data['byPrivate']){
        $by[] = "Adres prywatny";
    } else if($data['byCompany']){
        $by[] = "Adres Firmy";
    }
    $by = implode(", ", $by);


    echo "<div class='job_output' style='font-size: 1rem'>";
	echo <<<HTML
	<div><span style="font-weight: bold; width: 130px; display: inline-block">Zwrot przez:</span>$by</div>
HTML;
    if($data['byCompany'] || $data['byPrivate']){
            unset($data['byCompany']);
            unset($data['byPrivate']);
            unset($data['byEmail']);
            
            foreach($data as $property => $value){
                echo <<<HTML
                <div><span style="font-weight: bold; width: 130px; display: inline-block">$translation[$property]: </span>$value</div>
HTML;
            }

    }

echo "</div>";
}