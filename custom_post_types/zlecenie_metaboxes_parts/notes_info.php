<?php

function notes_meta_box($post){
    $notes = get_post_meta( $post->ID, 'notes_info', true);
    if(count($notes) > 0){
        foreach ($notes as $note) {
            echo "<b>".$note->date."</b>"."<br>".$note->text."<br>";
            echo <<<HTML
                <input name="$note->id" type="checkbox" value="yes" id="123123123" />
                <label for="123123123" style="color:red">Usuń notatkę</label> (przy następnym kliknięciu zaktualizuj)
                <hr>
HTML;
        }
    
    }

    echo <<<HTML
    <h4>Nowa notatka:</h4>
    <textarea name="new_note" placeholder="Wpisz nową notatkę a następnie kliknij aaktualizuj aby ją zapisać" style="width:100%"></textarea>


HTML;
}


function notes_meta_box_save($post_id){
    $notes = get_post_meta( $post_id, 'notes_info', true);

    for($i=0;$i<count($notes); $i++){
        if(isset($_POST[$notes[$i]->id]) && $_POST[$notes[$i]->id] == "yes"){
            unset($notes[$i]);
            $i--;
        }
    }

    if($_POST["new_note"] != ""){
        $notes[] = (object)[
            "id" => random_str(),
            "text" => $_POST["new_note"],
            "date" => date("Y-m-d H:i:s")
        ];
    }

    update_post_meta(
        $post_id,
        'notes_info',
        $notes
    );
}

add_action('save_post', 'notes_meta_box_save');
