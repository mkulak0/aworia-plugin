<?php
function files_meta_box($post){
    echo "<div style='font-size: 1.2rem'>Lista załączonych plików:</div>";
    $basepath = WP_CONTENT_DIR.(string)"/files/".$post->ID;
    $url_basepath = WP_CONTENT_URL.(string)"/files/".$post->ID;
    echo "<ul style='font-size:1rem'>";
    foreach(scandir($basepath) as $file){
        if($file != "." && $file != ".."){
            echo "<li><a href='$url_basepath/$file' download>$file</a></li>";
        }
    }
    echo "</ul>";
}