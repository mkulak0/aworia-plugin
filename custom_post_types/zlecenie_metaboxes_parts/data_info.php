<?php
function data_meta_box($post){
    $data = get_post_meta( $post->ID, 'data_info', true);
    $terms = get_the_terms($post->ID, 'delivery_type');

    foreach($terms as $term){
        if($term->slug == 'poczta'){
            echo "<div style='font-'>Zostaną przesłane pocztą</div>";
        } else if($term->slug == 'internet'){
            $text = $data['text'] != '' ? $data['text'] : "Użytkownik nie wprowadził tekstu";
            $text = nl2br($text);
            
            $langOutput = implode(", ", $data['languageOutput']);
            echo <<<HTML
            <div style='font-size: 1rem'>
                <div>
                    <span style="font-weight: bold">Język Wejściowy: </span>${data['languageInput']}
                </div>
                <div>
                    <span style="font-weight: bold">Języki Wyjściowe: </span>$langOutput
                </div>
            </div>
HTML;          

            echo <<<HTML
            
            <div style='font-size: 1rem'>
                <div style="font-size: 1.2rem; font-weight: bold">Tekst: </div>
                <textarea style="width:100%" name="text">$text</textarea>
            </div>
            <hr>
            <div style='font-size: 1rem'>
                <div style="font-weight: bold">Komentarz do tłumaczenia: </div>
                <textarea style="width:100%" name="comment">${data["comment"]}</textarea>
                
            </div>
HTML;
        }
    }
}

function data_meta_box_save($post_id){
    $data = get_post_meta( $post_id, 'data_info', true);
    $data["text"] = $_POST['text'];
    $data["comment"] = $_POST['comment'];
    update_post_meta(
        $post_id,
        'data_info',
        $data
    );
}
add_action('save_post', 'data_meta_box_save');