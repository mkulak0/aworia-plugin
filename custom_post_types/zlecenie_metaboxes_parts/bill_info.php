<?php

function bill_meta_box($post) {
    $data = get_post_meta( $post->ID, 'bill_info', true);
    $cost = $data['cost'];
    $translation = [
        'address' => 'Adres',
        'postCode' => 'Kod pocztowy',
        'city' => 'Miasto',
        'country' => 'Kraj',
        'phone' => 'Numer telefonu',
        'name' => 'Imię i Nazwisko',
        'ID' => 'PESEL',
        'cost' => 'Koszt'
    ];
    echo "<div class='job_output' style='font-size: 1rem'>";

    $transactionStatus = $data['transactionStatus'] ? "Opłacone" : "Nieopłacone";

    if($data['billActive']){
        unset($data['billActive']);
        foreach($data as $property => $value){
            echo <<<HTML
            <div><span style="font-weight: bold; width: 130px; display: inline-block">$translation[$property]: </span>$value</div>
HTML;
        }
    } else {
        echo "Bez faktury";
        echo <<<HTML
        <div><span style="font-weight: bold;">Koszt: </span>$cost zł</div>
HTML;
    }

    echo <<<HTML
    <div>
        <span style="font-weight: bold;">Status transakcji: </span>$transactionStatus
    </div>
HTML;


    echo "</div>";
}