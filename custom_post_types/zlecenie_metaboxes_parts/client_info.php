<?php

function client_info_meta_box($post){
    $data = get_post_meta( $post->ID, 'client_info', true);
    $email = $data['email'];
    $phone = $data['phone'];
    echo <<<HTML
    <div style="font-size: 1rem">
        <!-- <div><span style="font-weight: bold; width: 75px; display: inline-block">Email:</span>$email</div>
        <div><span style="font-weight: bold; width: 75px; display: inline-block">Telefon:</span>$phone</div> -->
        <label for="email">Email:</label>
        <input id="email" type="email" value="$email" name="email"/>
        <br>
        <label for="phone">Telefon:</label>
        <input id="phone" type="phone" value="$phone" name="phone" />
    </div>
HTML;
}

function client_info_meta_box_save($post_id){
    $array = ['email' => $_POST['email'], 'phone' => $_POST["phone"]];
    update_post_meta(
        $post_id,
        'client_info',
        $array
    );
}
add_action('save_post', 'client_info_meta_box_save');