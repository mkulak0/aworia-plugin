<?php

/**
* Plugin Name: Aworia Special Plugin
* Plugin URI:  
* Description: 
* Tags: 
* Author: Maciej Kułak
* Author URI: 
* Version: 0.0
* License: 
* License URI: 
* Text Domain: aworia-plugin
**/

define("AWORIA_PLUGIN_FILE", __FILE__);

//Initialize CPT
require_once( plugin_dir_path(__FILE__).'custom_post_types/jezyk.php');
require_once( plugin_dir_path(__FILE__).'custom_post_types/zlecenie.php');
require_once(plugin_dir_path( __FILE__ ).'custom_post_types/formularz.php');    

//Add default variables for zlecenie taxonomies
require_once(plugin_dir_path(__FILE__) . 'initialize.php');

require_once(plugin_dir_path(__FILE__) . 'post_handlers/zlecenie-handler.php');
require_once(plugin_dir_path(__FILE__) . 'post_handlers/file-handler.php');
require_once(plugin_dir_path(__FILE__) . 'post_handlers/form-handler.php');
require_once(plugin_dir_path(__FILE__) . 'post_handlers/payment-handler.php');
require_once(plugin_dir_path(__FILE__) . 'post_handlers/response-handler.php');

require_once(plugin_dir_path(__FILE__) . 'plugin_handlers/save-options-handler.php');
require_once(plugin_dir_path(__FILE__) . 'plugin_handlers/new-link-handler.php');

require_once(plugin_dir_path(__FILE__) . 'admin_panel/admin_panel.php');
require_once(plugin_dir_path(__FILE__) . 'admin_panel/advanced_search.php');





/* Functions */

function random_str(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
): string {
    if ($length < 1) {
        throw new \RangeException("Length must be a positive integer");
    }
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}




