<?php

add_action("admin_menu", "advanced_search");

function advanced_search(){
    add_menu_page( 'Wyszukiwanie zaawansowane', 'Wyszukiwanie zaawansowane', 'manage_options', 'aworia-search', 'aworia_search_view', "dashicons-palmtree");
}

function inside_search($name_of_metabox, $name_of_field, $searching_value){
    $wp_query = new WP_Query(['post_type' => 'job']);
    $jobs = $wp_query->posts;

    $output = [];

    foreach($jobs as $job){
        $meta = get_post_meta($job->ID, $name_of_metabox, true);
        if($meta[$name_of_field] == $searching_value){
            $output[] = $job;
        }
    }

    return $output;
}



function aworia_search_view(){
    $action = admin_url("admin.php?page=aworia-search");

    $jobs = [];

    echo <<<HTML
    <h1>Wyszukiwanie zaawansowane zleconych tłumaczeń</h1>
    <form method="post" action="$action">
        <h2>Dane zamawiającego</h2>
        <label for="phone">Numer telefonu: </label><input type="phone" name="phone" id="phone" style="margin:0.5rem" /><br>
        <label for="email">Adres e-mail: </label><input type="email" name="email" id="email" style="margin:0.5rem" />

        <h2>Sposób dostawy</h2>
        <label for="name">Imię i nazwisko: </label><input type="text" name="name" id="name" style="margin:0.5rem" /><br>
        <label for="address">Ulica oraz numer: </label><input type="text" name="address" id="address" style="margin:0.5rem"  /><br>
        <label for="city">Miejscowość: </label><input type="text" name="city" id="city" style="margin:0.5rem" /> <br>

        <button type="submit">Wyszukaj</button>
    </form>
    <br>
HTML;

    if(@$_POST['phone'] != ""){
        $jobs = array_merge($jobs, inside_search("client_info", "phone", $_POST['phone']));
    }
    if(@$_POST['email'] != ""){
        $jobs = array_merge($jobs, inside_search("client_info", "email", $_POST['email']));
    }

    if(@$_POST['name'] != ""){
        $jobs = array_merge($jobs, inside_search("delivery_info", "name", $_POST['name']));
    }
    if(@$_POST['address'] != ""){
        $jobs = array_merge($jobs, inside_search("delivery_info", "address", $_POST['address']));
    }
    if(@$_POST['address'] != ""){
        $jobs = array_merge($jobs, inside_search("delivery_info", "address", $_POST['address']));
    }

    //print_r($jobs);
    echo "<h2>Wyniki wyszukiwania</h2>";

    foreach($jobs as $job){
        $link = admin_url("post.php?post=".$job->ID."&action=edit");
        echo <<<HTML
        <a href="$link">$job->post_title</a><br>
HTML;
    }


    
}