<?php

add_action('admin_menu', 'aworia_menu');
 
function aworia_menu(){
        add_menu_page( 'Aworia Ustawienia', 'Aworia Płatności', 'manage_options', 'aworia-plugin', 'aworia_view', "dashicons-palmtree");
}
 
function aworia_view(){
        $form_link = admin_url('admin-ajax.php')."?action=save_options";
        $new_link_link = admin_url('admin-ajax.php')."?action=new_link";

        $aworia_id = get_option('aworia_id');
        $aworia_pin = get_option('aworia_pin');
        $aworia_env = get_option('aworia_env');
        $aworia_login = get_option("aworia_login");
        $aworia_password = openssl_decrypt(get_option("aworia_password"), "AES-256-OFB", "DUPA");

        
        $plugin_data = get_plugin_data(AWORIA_PLUGIN_FILE);
        $plugin_author = $plugin_data['Author'];
        $plugin_name = $plugin_data['Name'];
        $plugin_folder = basename( plugin_dir_path(  dirname( __FILE__ , 1 ) ) );

        $recent_post_id = wp_get_recent_posts(array('post_type'=>'job'))[0]["ID"];


        echo <<<HTML
        <h1>Ustawienia płatności dla strony Aworia</h1>

        <!-- Dotpay -->
        <h3>Dane do systemu Dotpay</h3>
        <form method="post" action="$form_link">
            ID Sklepu: <input type="number" value="$aworia_id" name="aworia_id" style="margin:0.5rem" /><br>
            Pin do sklepu: <input type="text" value="$aworia_pin" name="aworia_pin" style="margin:0.5rem" /><br>

            Login: <input type="text" value="$aworia_login" name="aworia_login" style="margin:0.5rem" /><br>
            Hasło: <input type="password" value="$aworia_password" name="aworia_password" style="margin:0.5rem" /><br>
            Środowisko (production/test): <input type="text" value="$aworia_env" name="aworia_env" style="margin:0.5rem" /><br>
            <button type="submit">Zaaktualizuj dane</button>
        </form>

        <br><hr>

        <h3>Generowanie linku płatniczego</h3>
        <form method="post" action="$new_link_link">
            Identyfikator zlecenia (wpisane id to id ostatniego zlecenia): <input type="number" name="control" value="$recent_post_id" style="margin:0.5rem" /><br>
            Opłata (pełna liczba, bez przecinków): <input type="number" name="amount" value="10" style="margin:0.5rem" /><br>
            Tytuł: <input type="text" name="description" value="Zapłata za tłumaczenie" style="margin:0.5rem" /><br>
            <button type="submit">Wygeneruj link płatniczy</button><br>
            <b><span style="color:red">Uwaga! Generowanie linku może chwilę potrwać w zależności od obciążenia serwerów Dotpay</span></b>
        </form>
        <br><hr>

        <!-- Dev -->
        <h3>Informacje i ustawienia developerskie</h3>
        <h4>Nazwa pluginu: $plugin_name</h4>
        <h4>Autor: $plugin_author</h4>
        <h4>Nazwa folderu: $plugin_folder</h4>
        <h4>Plugin włączony pomyślnie</h4>


HTML;
}