<?php
function initialize(){
    $tab = [
        'delivery_type' => [
            'Internet',
            'Poczta'
        ],
        'priority' => [
            'Ekspresowy',
            'Pilny',
            'Przyspieszony',
            'Standardowy'
        ],
        'special_translation' => [
            'Nie',
            'Nie wiem',
            'Tak',
            'Brak'
        ],
        'type_of_translation' => [
            'Nie wiem',
            'Przysięgłe',
            'Zwykłe',
            'Brak'
        ],
    ];

    foreach($tab as $key => $value){
        foreach($value as $name){
            if(term_exists($name, $key) === null){
                wp_insert_term($name, $key);
            }
        }
    }

    add_option("aworia_id", 0);
    add_option("aworia_pin", "");
    add_option("aworia_env", "test");
    add_option("aworia_login", "");
    add_option("aworia_password", "");
}
add_action('init', 'initialize');